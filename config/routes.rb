Rails.application.routes.draw do

  unauthenticated do
    as :user do
      root to: 'users/sessions#new'
    end
  end
  
  root 'static_pages#home'
  
  devise_for :users,controllers: {
    sessions:      "users/sessions",
    registrations: "users/registrations",
    passwords:     "users/passwords" 
  }
  
  
  
 resources :users, only: [:index, :show] 
 resources :microposts, only: [:create, :destroy]
    
end
