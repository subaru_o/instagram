class DropUser < ActiveRecord::Migration[5.1]
  def up
    drop_table :users
  end
  
  def down
     create_table :users do |t|
      t.string :name
      t.string :email,unique: true
      t.string :password_digest
      t.timestamps
     end
  end
end
