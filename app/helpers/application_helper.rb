module ApplicationHelper
  
  def full_title(title= "" )
    if title.empty?
      "Instagram"
    else
      "#{title} | Instagram"
    end
  end
  
  def current_user?(user)
    user = current_user
  end
  
end
